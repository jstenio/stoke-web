package com.stoke.web.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by Stênio on 13/02/2016.
 */
public class CryptManager {

    public static String encrypt(String password){
        MessageDigest digest = null;
        try {
            if(password!=null){
                digest = MessageDigest.getInstance("MD5");
                byte[] bytes = digest.digest(password.getBytes("UTF-8"));
                StringBuilder builder = new StringBuilder();
                for(byte b:bytes){
                    builder.append(String.format("%02x",0xFF & b));
                }
                return builder.toString();
            }else{
                System.out.println("senha é nula");
                return null;
            }

        } catch (NoSuchAlgorithmException e) {
            return password;
        } catch (UnsupportedEncodingException e) {
            return password;
        }
    }

    public static String generateCodigo(){
        Random random = new Random();

        int size = random.nextInt(4) + 8;
        System.out.println("size: "+size);
        String password = "";
        for (int i = 0; i<size; i++){
            int index = generateIndex(random);
            System.out.println("valor: "+index+" char: "+((char)index));
            password = password.concat(((char)index)+"");
        }
        return password;
    }

    protected static int generateIndex(Random random){
        int index = random.nextInt(85) +60;
        if((index>47&&index<58)||(index>96&&index<123))
            return index;
        else return generateIndex(random);
    }
}
