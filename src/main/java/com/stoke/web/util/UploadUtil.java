package com.stoke.web.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;

import com.stoke.persistence.entity.Produto;

public class UploadUtil {
	
	public static String createImagem(UploadedFile file,Produto produto,FacesContext context){
		if(file!=null){
			String path = context.getExternalContext().getRealPath("../images/produtos");
			path += path.endsWith(File.separator)?"":File.separator;
			String names[] = file.getFileName().split(".");
			String ext = "jpg";
			if(names.length>1){
				System.out.println(">>>1");
				ext = names[1];
			}
			System.out.println("path: "+path+file.getFileName());
		 
			String name = produto.getNome()+"-"+produto.getCodigo();
			String imagemName = name+"."+ext;
			try {
				InputStream input = file.getInputstream();
				if(input!=null){
					int size = (int)file.getSize();
					int read = 0;
					
					FileOutputStream out = new FileOutputStream(path+imagemName);
					byte bytes[] = new byte[size];
					while((read = input.read(bytes))!=-1){
						out.write(bytes,0,read);
					}
					out.flush();
					out.close();
					context.addMessage(null, new FacesMessage("Filename: "+file.getFileName()));
					System.out.println("Filename: "+imagemName);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			produto.setImagem(imagemName);
//			BufferedWriter writer = new BufferedWriter()			
		}else System.out.println("File nulo");
		return null;
	}
	
}
