package com.stoke.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.stoke.persistence.DAO;
import com.stoke.persistence.entity.Admin;
import com.stoke.persistence.entity.Cliente;
import com.stoke.persistence.entity.Produto;
import com.stoke.persistence.entity.ProdutoItem;
import com.stoke.persistence.entity.Venda;

@ManagedBean
@SessionScoped
public class CarrinhoBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private Set<ProdutoItem> itens;
	private ProdutoItem item;
	
	public CarrinhoBean() {
		itens = new HashSet<ProdutoItem>();
		item = new ProdutoItem();
	}
	
	public void addItem(){
		itens.add(item);
		item = new ProdutoItem();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Item adicionado com sucesso!"));
	}
	public Cliente logado(){
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		Cliente cliente = (Cliente)session.getAttribute("cliente");
		return cliente;
	}
	
	public Admin logadoAdmin(){
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		Admin admin = (Admin)session.getAttribute("admin");
		return admin;
	}
	
	public String logout(){
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		session.invalidate();
		return "/login.xhtml?faces-redirect=true;";
	}
	
	public void finalizarVenda(){
		Cliente cliente = logado();
		if(cliente!=null){
			Venda venda = new Venda();
			venda.setCliente(cliente);
			venda.setData(Calendar.getInstance());
			if(!itens.isEmpty()){
			for(ProdutoItem it:itens){
				it.setVenda(venda);
				if(it.getProduto()!=null){
					int q1 = it.getProduto().getQuantidade()==null?0:it.getProduto().getQuantidade();
					int q2 = it.getQuantidade()==null?0:it.getQuantidade();
					it.getProduto().setQuantidade(q1-q2);
					new DAO<Produto>(Produto.class).update(it.getProduto());
					if(venda.getItens()==null)
						venda.setItens(new ArrayList<ProdutoItem>());
					venda.getItens().add(it);
				}
			}
			new DAO<Venda>(Venda.class).insert(venda);
			}else{ 
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Adicione pelo menos um item ao carrinho!",""));
			}
			itens = new HashSet<ProdutoItem>();
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "É necessário estar logado para finalizar uma compra!",""));
		}
	}
	public boolean removerItem(ProdutoItem item){
		if(itens!=null){
			return itens.remove(item);
		}
		return false;
	}
	public List<ProdutoItem> getItens() {
		return new ArrayList<ProdutoItem>(itens);
	}

	public void setItens(Set<ProdutoItem> itens) {
		this.itens = itens;
	}

	public ProdutoItem getItem() {
		return item;
	}

	public void setItem(ProdutoItem item) {
		this.item = item;
	}
	
	
	
}
