package com.stoke.web.bean; 
import java.util.Arrays;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.stoke.persistence.DAO;
import com.stoke.persistence.entity.Admin;
import com.stoke.persistence.entity.Cliente;
import com.stoke.persistence.entity.Usuario;
import com.stoke.persistence.util.Parameter; 

@ManagedBean 
public class LoginBean {
	
	private String login; 
	private String senha; 
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String validar(){
		Parameter param1 = new Parameter("pEmail", login);
		Parameter param2 = new Parameter("pSenha", senha);
		Usuario usuario = new DAO<Usuario>(Usuario.class).findByQuery("userByEmailAndPassword", Arrays.asList(param1,param2));
		if(usuario!=null){
			Cliente cliente = new DAO<Cliente>(Cliente.class).findbyId(usuario.getId());
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			HttpSession session = request.getSession();
			if(cliente!=null){
				session.setAttribute("cliente", cliente);
				return "/index.xhtml?faces-redirect=true";
			}else{
				Admin admin = new DAO<Admin>(Admin.class).findbyId(usuario.getId());
				session.setAttribute("admin", admin);
				return "/admin.xhtml?faces-redirect=true";
			}
//			return "/index.xhtml?faces-redirect=true";
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Email ou senha inválidos!"));
			return null;
		}
	} 
	
}