package com.stoke.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.stoke.persistence.DAO;
import com.stoke.persistence.entity.Cliente;
import com.stoke.persistence.entity.Endereco;
import com.stoke.persistence.entity.Telefone;

@ManagedBean
@ViewScoped
public class ClienteBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Cliente novoCliente;
	private Telefone telefone;
	private List<Cliente> clientes;
	
	public ClienteBean() {
		this.novoCliente = new Cliente();
		this.telefone = new Telefone();
		this.novoCliente.setTelefones(new ArrayList<Telefone>());
		this.novoCliente.setEndereco(new Endereco());
		this.clientes = new DAO<Cliente>(Cliente.class).listAll();
	}

	public void cadastrar(){
		if(novoCliente.getTelefones()!=null)
			if(novoCliente.getTelefones().isEmpty())
				novoCliente.setTelefones(null);
		novoCliente.getEndereco().setCliente(novoCliente);
		new DAO<Cliente>(Cliente.class).insert(novoCliente);
		novoCliente = new Cliente();
	}
	
	public void addTel(){
		if(novoCliente.getTelefones()==null)
			novoCliente.setTelefones(new ArrayList<Telefone>());
		if(telefone.getNumero()!=null){
			if(!telefone.getNumero().isEmpty()){
				telefone.setCliente(novoCliente);
				novoCliente.getTelefones().add(telefone);
			}
		}
		telefone = new Telefone();
	}
	
	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public Cliente getNovoCliente() {
		return novoCliente;
	}

	public void setNovoCliente(Cliente novoCliente) {
		this.novoCliente = novoCliente;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
}
