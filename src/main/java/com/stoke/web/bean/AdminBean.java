package com.stoke.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stoke.persistence.DAO;
import com.stoke.persistence.entity.Admin;
import com.stoke.persistence.entity.Cliente;
import com.stoke.persistence.entity.Telefone;

@ManagedBean
@ViewScoped
public class AdminBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Admin novoAdmin;
	private List<Cliente> clientes;
	private Cliente clienteEdit;
	private Telefone telefone;
	
	public AdminBean(){
		novoAdmin = new Admin();
		telefone = new Telefone();
		clienteEdit = new Cliente();
		clientes = new DAO<Cliente>(Cliente.class).listAll();
	}
	
	public void cadastrar(){
		new DAO<Admin>(Admin.class).insert(novoAdmin);
		novoAdmin = new Admin();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Admin criado com sucesso!", ""));
	}
	
	public void editarCliente(){
		if(clienteEdit.getTelefones()!=null){
			if(clienteEdit.getTelefones().isEmpty())
				clienteEdit.setTelefones(null);
		}
		new DAO<Cliente>(Cliente.class).update(clienteEdit);
		clienteEdit = new Cliente();
	}
	
	public void addTel(){
		if(clienteEdit.getTelefones()==null)
			clienteEdit.setTelefones(new ArrayList<Telefone>());
		if(telefone.getNumero()!=null){
			if(!telefone.getNumero().isEmpty()){
				telefone.setCliente(clienteEdit);
				clienteEdit.getTelefones().add(telefone);
			}
		}
		telefone = new Telefone();
	}
	
	public void buscarClientes(){
		clientes = new DAO<Cliente>(Cliente.class).listAll();
	}
	
	public Admin getNovoAdmin() {
		return novoAdmin;
	}

	public void setNovoAdmin(Admin novoAdmin) {
		this.novoAdmin = novoAdmin;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public Cliente getClienteEdit() {
		return clienteEdit;
	}

	public void setClienteEdit(Cliente clienteEdit) {
		this.clienteEdit = clienteEdit;
	}

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	
	
}
