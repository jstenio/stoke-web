package com.stoke.web.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.stoke.persistence.DAO;
import com.stoke.persistence.entity.Produto;
import com.stoke.web.util.CryptManager;
import com.stoke.web.util.UploadUtil;

@ManagedBean
@ViewScoped
public class ProdutoBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<Produto> produtos;
	private Produto novoProduto;
	private Produto selectProduto;
	private UploadedFile file;
	
	public ProdutoBean(){
		novoProduto = new Produto();
		produtos = new DAO<Produto>(Produto.class).listAll();
	}
	
	public List<Produto> getProdutos() {
		return produtos;
	}

	public UploadedFile getFile() {
		System.out.println("pegando arquivo");
		return file;
	}

	public void setFile(UploadedFile file) {
		System.out.println("setando arquivo");
		this.file = file;
	}
	
	public void criarProduto(){
		if(novoProduto!=null){
			String codigo = CryptManager.generateCodigo();
			novoProduto.setCodigo(codigo);
			UploadUtil.createImagem(this.getFile(), novoProduto,FacesContext.getCurrentInstance());
			System.out.println("Codigo: "+novoProduto.getCodigo()+"\nimagem: "+novoProduto.getImagem());
			new DAO<Produto>(Produto.class).insert(novoProduto);
			
			novoProduto = new Produto();
		}
	}
	
	public void upload(FileUploadEvent event){
		if(event!=null){
			System.out.println("Recebendo.. "+event.getFile().getFileName());
			this.file = event.getFile();
		}else System.out.println("evento nulo");
	}
	
	public void doUpload(FileUploadEvent event){
		if(event!=null){
			UploadedFile file = event.getFile();
			if(file!=null){
				System.out.println("file: "+file.getFileName());
				try {
					String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("../images/produtos");
					path += path.endsWith(File.separator)?"":File.separator;
					System.out.println("path: "+path);
					FileOutputStream out = new FileOutputStream(path+"imgt.jpg");
					int size = (int)file.getSize();
					InputStream input = file.getInputstream();
					int read = 0;
					byte bs[] = new byte[size];
					while((read=input.read(bs))!=-1)
						out.write(bs, 0, read);
					out.flush();
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
	}

	public Produto getNovoProduto() {
		return novoProduto;
	}

	public void setNovoProduto(Produto novoProduto) {
		this.novoProduto = novoProduto;
	}

	public Produto getSelectProduto() {
		return selectProduto;
	}

	public void setSelectProduto(Produto selectProduto) {
		this.selectProduto = selectProduto;
	}
	
}
