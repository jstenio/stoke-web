package com.stoke.web.bean;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class IndexBean {
	private String valor = "Teste";

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
